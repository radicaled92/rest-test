<?php

declare(strict_types=1);

namespace App\Api\Travel\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Api\Travel\Entity\Travel;
/**
 * @ORM\Entity
 * @ApiResource(
 *  itemOperations={
 *      "get"
 *  },
 *  collectionOperations={
 *      "post",
 *      "get"={"pagination_enabled"=false}
 *  }
 * )
 */
class Country
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string")
     */
    private string $name;

    /**
     * @ORM\Column(type="string")
     */
    private string $countryCode;

    /**
     * @ORM\ManyToMany(targetEntity=Travel::class, mappedBy="countries")
     */
    private Collection $travels;

    public function __construct()
    {
        $this->travels = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getCountryCode(): string
    {
        return $this->countryCode;
    }

    public function setCountryCode(string $countryCode): self
    {
        $this->countryCode = $countryCode;
        return $this;
    }

    public function getTravels(): Collection
    {
        return $this->travels;
    }

    public function addTravel(Travel $travel): self
    {
        if (!$this->travels->contains($travel))
        {
            $this->travels->add($travel);

        }
        return $this;
    }

    public function removeTravel(Travel $travel): self
    {
        if($this->travels->contains($travel))
        {
            $this->travels->removeElement($travel);

        }
        return $this;
    }

}