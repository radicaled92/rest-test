<?php

declare(strict_types=1);

namespace App\Api\Travel\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Api\Travel\Entity\Country;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\ORM\Mapping\Index;

/**
 * @ORM\Entity
 * @ORM\Table(indexes={@Index(name="season_idx", columns={"season"})})
 * @ApiResource(
 *  itemOperations={
 *      "get"
 *  },
 *  collectionOperations={
 *      "post",
 *      "get"={"pagination_enabled"=false}
 *  }
 * )
 * @ApiFilter(SearchFilter::class, properties={"season":"exact"})
 */
class Travel
{

    const WINTER = 'winter';
    const AUTUMN = 'autumn';
    const SPRING = 'spring';
    const SUMMER = 'summer';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string")
     */
    private string $title;

    /**
     * @ORM\Column(type="date_immutable")
     */
    private \DateTimeImmutable $startDate;

    /**
     * @ORM\Column(type="date_immutable")
     */
    private \DateTimeImmutable $endDate;

    /**
     * @ORM\ManyToMany(targetEntity=Country::class, inversedBy="travels", fetch="EAGER")
     * @ORM\JoinTable(name="travels_countries")
     */
    private Collection $countries;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $season;

    public function __construct()
    {
        $this->countries = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;
        return $this;
    }

    public function getStartDate(): \DateTimeImmutable
    {
        return $this->startDate;
    }

    public function setStartDate(\DateTimeImmutable $startDate): self
    {
        $this->startDate = $startDate;
        return $this;
    }

    public function getEndDate(): \DateTimeImmutable
    {
        return $this->endDate;
    }

    public function setEndDate(\DateTimeImmutable $endDate): self
    {
        $this->endDate = $endDate;
        return $this;
    }

    public function getCountries(): Collection
    {
        return $this->countries;
    }

    public function addCountry(Country $country): self
    {
        if (!$this->countries->contains($country))
        {
            $this->countries->add($country);

        }
        return $this;
    }

    public function removeCountry(Country $country): self
    {
        if ($this->countries->contains($country))
        {
            $this->countries->removeElement($country);

        }
        return $this;
    }

    public function getSeason(): ?string
    {
        return $this->season;
    }

    public function setSeason(?string $season): self
    {
        $this->season = $season;
        return $this;
    }

}