<?php

declare(strict_types=1);

namespace App\Mono\Controller;

use App\Api\Travel\Entity\Travel;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class DefaultController
{

    private $twig;
    private $entityManager;

    public function __construct(Environment $twig, EntityManagerInterface $entityManager)
    {
        $this->twig = $twig;
        $this->entityManager = $entityManager;
    }
    /**
     * @Route("/varnish-test", name="varnish_test")
     */
    public function varnishTest(): Response
    {

        // get winter travel
        $winterTravel = $this->entityManager->getRepository(Travel::class)->findBy(['season' => Travel::WINTER]);


        return new Response($this->twig->render('varnishTest.html.twig', ['travels' => $winterTravel]));
    }
}