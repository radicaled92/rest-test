<?php

namespace App\DataFixtures;

use App\Api\Travel\Entity\Country;
use App\Api\Travel\Entity\Travel;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);

        for ($i = 0; $i < 5000; $i++)
        {
            $travel = new Travel();
            $travel->setTitle('travel'. $i);
            $travel->setEndDate(new \DateTimeImmutable());
            $travel->setStartDate(new \DateTimeImmutable());

            // season
            switch ($i){
                case $i < 1250 : $travel->setSeason(Travel::SPRING);
                break;
                case $i < 2500 : $travel->setSeason(Travel::SUMMER);
                break;
                case $i < 3750 : $travel->setSeason(Travel::AUTUMN);
                break;
                case $i < 5000 : $travel->setSeason(Travel::WINTER);
                break;
            }

            $country = new Country();
            $country->setName('country'. $i);
            $country->setCountryCode('CODE'. $i);
            $manager->persist($country);

            $travel->addCountry($country);
            $manager->persist($travel);
        }

        $manager->flush();
    }
}
